<?php

namespace yagerguo\yii2ad\queries;

use yagerguo\yii2ad\models\AdItem;

/**
 * This is the ActiveQuery class for [[\common\models\Product]].
 *
 * @see \common\models\Product
 */
class ItemQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        $this->andWhere(['status' => AdItem::STATUS_ACTIVE]);
        return $this;
    }
    
    public function findOne($condition){
        return $this->active()->andWhere($condition)->one();
    }
    
    public function findAll($condition){
        return $this->active()->andWhere($condition)->all();
    }
    
}