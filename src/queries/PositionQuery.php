<?php

namespace yagerguo\yii2ad\queries;

use yagerguo\yii2ad\models\AdPosition;

/**
 * This is the ActiveQuery class for [[\common\models\Product]].
 *
 * @see \common\models\Product
 */
class PositionQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        $this->andWhere(['status' => AdPosition::STATUS_ACTIVE]);
        return $this;
    }
    
    public function findOne($condition){
        return $this->active()->andWhere($condition)->one();
    }
    
}