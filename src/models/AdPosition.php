<?php

namespace yagerguo\yii2ad\models;

use Yii;

/**
 * This is the model class for table "ad_position".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property integer $type
 * @property integer $status
 * @property string $class
 */
class AdPosition extends \yii\db\ActiveRecord
{
    
    const STATUS_ACTIVE = 1;
    const STATUS_DISABLED = 0;
    
    const TYPE_SINGLE = 1;
    const TYPE_MULTIPLE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ad_position';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug', 'type'], 'required'],
            [['type', 'status'], 'integer'],
            [['name', 'slug', 'class'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => '名称',
            'slug' => 'Slug',
            'type' => '类型',
            'typeText' => '类型',
            'status' => '状态',
            'statusText' => '状态',
            'class' => 'Class',
        ];
    }
    
    public static function find() {
        return new \yagerguo\yii2ad\queries\PositionQuery(get_called_class());
    }
    
    public static function statusData(){
        return [
            self::STATUS_ACTIVE => '开启',
            self::STATUS_DISABLED => '关闭'
        ];
    }
    
    public static function typeData(){
        return [
            self::TYPE_SINGLE => '单条',
            self::TYPE_MULTIPLE => '多条'
        ];
    }
    
    public function getStatusText(){
        return self::statusData()[$this->status];
    }
    
    public function getTypeText(){
        return self::typeData()[$this->type];
    }
    
    public static function getData($slug){
        $position = self::find()->findOne(['slug' => $slug]);
        if($position->type == self::TYPE_SINGLE){
            $data = AdItem::find()->findOne(['adPositionId' => $position->id]);
        }else{
            $data = AdItem::find()->orderBy('sort desc')->findAll(['adPositionId' => $position->id]);
        }
        return $data;
    }
}
