<?php

namespace yagerguo\yii2ad\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "ad_item".
 *
 * @property integer $id
 * @property integer $adPositionId
 * @property string $name
 * @property integer $type
 * @property string $text
 * @property string $image
 * @property string $link
 * @property integer $status
 */
class AdItem extends \yii\db\ActiveRecord
{    
    
    const STATUS_ACTIVE = 1;
    const STATUS_DISABLED = 0;
    
    public $picFile;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ad_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['adPositionId', 'name'], 'required'],
            [['adPositionId', 'type', 'status', 'sort'], 'integer'],
            [['picFile'], 'file'],
            [['text'], 'string'],
            [['name', 'image', 'link'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'adPositionId' => 'Ad Position ID',
            'name' => '名称',
            'type' => '类型',
            'text' => '文字内容',
            'image' => '图片',
            'picFile' => '图片',
            'link' => '链接',
            'status' => '状态',
            'statusText' => '状态',
            'sort' => '排序'
        ];
    }
    
    public static function find() {
        return new \yagerguo\yii2ad\queries\ItemQuery(get_called_class());
    }
    
    public static function statusData(){
        return [
            self::STATUS_ACTIVE => '开启',
            self::STATUS_DISABLED => '关闭'
        ];
    }
    
    public function getStatusText(){
        return self::statusData()[$this->status];
    }
    
    public function process($folder, $site){
       
        $this->save();
       
        // 更新图片
        $fileName  = 'picFile';
        $this->$fileName = UploadedFile::getInstance($this, $fileName);
        if ($this->$fileName && $this->validate()) {
            $path = time() . rand(1000, 9999) . '.' . $this->$fileName->extension;
            $this->$fileName->saveAs( $folder . $path);
            $this->image = $site . $path;
            $this->save();
        }
        
    }
    
}
