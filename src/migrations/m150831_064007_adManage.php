<?php

use yii\db\Schema;
use yii\db\Migration;

class m150831_064007_adManage extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%ad_position}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'slug' => Schema::TYPE_STRING . ' NOT NULL',
            'type' => Schema::TYPE_INTEGER,
            'status' => Schema::TYPE_INTEGER,
            'class' => Schema::TYPE_STRING,
        ], $tableOptions);
        
        $this->createTable('{{%ad_item}}', [
            'id' => Schema::TYPE_PK,
            'adPositionId' => Schema::TYPE_INTEGER . ' NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'type' => Schema::TYPE_INTEGER,
            'text' => Schema::TYPE_TEXT,
            'image' => Schema::TYPE_STRING,
            'link' => Schema::TYPE_STRING,
            'status' => Schema::TYPE_INTEGER,
        ], $tableOptions);
        
    }

    public function down()
    {
        echo "m150831_064007_adManage cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
