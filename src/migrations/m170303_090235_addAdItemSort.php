<?php

use yii\db\Migration;

class m170303_090235_addAdItemSort extends Migration
{
    public function up()
    {
        $this->addColumn('ad_item', 'sort', $this->integer());
    }

    public function down()
    {
        echo "m170303_090235_addAdItemSort cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
