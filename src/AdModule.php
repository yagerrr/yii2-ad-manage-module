<?php

namespace yagerguo\yii2ad;

class AdModule extends \yii\base\Module
{
    
    public $controllerNamespace = 'yagerguo\yii2ad\backend\controllers';
    
    public $uploadsFolder;
    
    public $imageUrl;
    
    public function init()
    {
        parent::init();
        
        if(empty($this->uploadsFolder)){
            $this->uploadsFolder = Yii::getAlias('@uploads') . '/ad/';
        }
    }
}
