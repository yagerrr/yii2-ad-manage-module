<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AdItem */

$this->title = '添加广告';
$this->params['subTitle'] = $position->name;
$this->params['breadcrumbs'][] = ['label' => 'Ad Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ad-item-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
