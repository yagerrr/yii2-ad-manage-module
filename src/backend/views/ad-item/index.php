<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ad Items';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ad-item-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ad Item', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'adPositionId',
            'name',
            'type',
            'text:ntext',
            // 'image',
            // 'link',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
