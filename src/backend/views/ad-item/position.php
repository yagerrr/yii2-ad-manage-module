<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\AdItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $position->name;
$this->params['subTitle'] = '广告管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ad-item-index">

    <div class="well">
        <?= Html::a('添加广告', ['create', 'adPositionId' => $position->id], ['class' => 'btn btn-success', 'target' => '_blank']) ?>
    </div>
    
    <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
            
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [

                    'image:image',
                    'name',
                    'type',
                    'text:ntext',
                    'link',
                    'statusText',
                    'sort',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
            
        </div>
    </div>

</div>