<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AdItem */

$this->title = $model->name;
$this->params['subTitle'] = '修改广告';
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = '修改';
?>
<div class="ad-item-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
