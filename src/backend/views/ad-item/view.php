<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\AdItem */

$this->title = $model->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ad-item-view">

    <div class="well">
        <?= Html::a('列表', ['position', 'id' => $model->adPositionId], ['class' => 'btn btn-default']) ?>
        <?= Html::a('修改', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('删除', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'adPositionId',
            'name',
            'type',
            'text:ntext',
            'image:image',
            'link',
            'status',
        ],
    ]) ?>

</div>
