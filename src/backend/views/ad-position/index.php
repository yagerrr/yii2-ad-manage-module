<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '广告位管理';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ad-position-index">

    <div class="well">
        <?= Html::a('添加广告位', ['create'], ['class' => 'btn btn-success']) ?>
    </div>
    
    <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [

                    'id',
                    'name',
                    'slug',
                    'typeText',
                    'statusText',
                    [
                        'label' => '管理',
                        'format' => 'raw',
                        'value' => function ($model){
                            return Html::a('管理广告', ['manage', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm', 'target' => '_blank']);
                        }
                    ],
                    // 'class',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>

</div>
