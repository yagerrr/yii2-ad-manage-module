<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AdPosition */

$this->title = $model->name;
$this->params['subTitle'] = '修改';
$this->params['breadcrumbs'][] = ['label' => '广告位', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = '修改';
?>
<div class="ad-position-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
